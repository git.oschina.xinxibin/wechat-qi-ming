# 起名小程序

#### 介绍
微信起名小程序，内有宝宝起名，公司起名及商标起名。

#### 软件架构
使用K86(快接口)的API制作而已
K86官网: [K86官网地址](https://www.k86.cn/)


#### 软件截图
![宝宝起名](https://images.gitee.com/uploads/images/2021/0720/164319_6000e728_355125.jpeg "WechatIMG1.jpeg")
![宝宝起名结果](https://images.gitee.com/uploads/images/2021/0720/164336_81b94fd9_355125.jpeg "WechatIMG6.jpeg")
![公司起名](https://images.gitee.com/uploads/images/2021/0720/164405_61200bb5_355125.jpeg "WechatIMG2.jpeg")
![公司起名结果](https://images.gitee.com/uploads/images/2021/0720/164419_0a3808c8_355125.jpeg "WechatIMG5.jpeg")
![商标起名](https://images.gitee.com/uploads/images/2021/0720/164437_014ad6a4_355125.jpeg "WechatIMG3.jpeg")
![商标起名结果](https://images.gitee.com/uploads/images/2021/0720/164449_f4589a3b_355125.jpeg "WechatIMG4.jpeg")
